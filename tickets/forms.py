from django.forms import ModelForm
from django.forms import fields
from django.utils.translation import gettext_lazy as _

from .models import Ticket


class TicketForm(ModelForm):
    class Meta:
        model = Ticket
        fields = "__all__"


class CreateTicketForm(ModelForm):
    class Meta:
        model = Ticket
        fields = ["number", "sat_begin", "sat_end", "respirator", "oxygen", "child", "hospital"]

    def save(self, commit=True):
        hospital = self.cleaned_data["hospital"]
        child = self.cleaned_data["child"]
        respirator = self.cleaned_data["respirator"]
        oxygen = self.cleaned_data["oxygen"]
        beds = hospital.bed_set.filter(child=child, oxygen=oxygen, respirator=respirator)
        if beds.count():
            obj = super().save(commit=False)
            obj.bed = beds[0]
            obj.save()
            return obj

